from distutils.core import setup

setup(
    name='HABsim',
    version='0.1',
    packages=['habsim'],
    url='',
    license='GPL v3.0',
    author='Jerald Thomas',
    author_email='jeraldamo@gmail.com',
    description='HABsim is a simulation tool for High Altitude Ballooning.'
)
