## Synopsis

HABsim is a simulation tool for High Altitude Ballooning.

It determines the following properties based on time iterative calculations:

>- Altitude
- Volume
- Radius
- Velocity
- Terminal Velocity

as well as calculating:

>- Launch volume
- Burst altitude
- Gas mass

It does this with the following as inputs:

>- Balloon burst diameter
- Balloon drag coefficient
- Balloon mass
- Payload mass
- Net positive mass

HABsim currently makes the following assumptions:

>- The ideal gas law holds
- The balloons have no permeability
- The balloons are perfectly spherical
- The system mass is known as inital conditions and are constant

HABsim also provides a GUI that allows you to change the input values and see real time graphs of the output data.

## Version 0.1 (Beta Release) (Current Release)

Version 0.1 is the prototype if you will. It was released in an attempt to garner feedback. It should be known that this release is unstable with minimal documentation and no external data verification.

Features:

>- Balloon flight simulation
- Atmosphere model generated from the U.S. Standard Atmosphere Model
- GUI

## Version 1.0

Version 1.0 aims to be the first stable release. The idea is that one could use this to determine payload and balloon characteristics for desired outcomes and then use an existing flight planner to determine launch location and time.

Goals:

>- Implement descent simulation
- Implement balloon permeability
- Optimize simulation calculations (pure python)

## Version 2.0

Version 2.0 aims to be the only software needed for HAB flight planning. With 2.0 one could optimize payload and balloon characteristics as well as run pre-flight simulations that use forecasted atmosphere models and GIS data to determine launce location and time.

Goals:

>- Implement NOAA or WRF forecasting for more accurate atmosphere models
- Implement GIS and mapping services
- Write core simulation in C and create libraries for Linux, Mac, and Windows (also keep an updated pure python version)

## Motivation

HABsim is being developed in an attempt to provide a local (offline) HAB simulator that is more than a calculator.

## Installation

As is the cass with all Python modules, installation of HABsim is not required as long as you run your program from the HABsim directory. Although the GUI does rely on the following packages:

>- Kivy 1.9.0
- KivyGarden-graph

If you would like to install HABsim simply run 'python setup.py install' with administrator privileges.

## Contributors

Currently the only contributor to the project is Jerald Thomas, <jeraldamo@gmail.com>.
Although if you are interested in contributing please let me know as I am eager to have fresh input on the project.

## License

HABsim is licensed under the GPL 3.0. For more information please read the LICENSE file or visit <http://www.gnu.org/licenses/>

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.