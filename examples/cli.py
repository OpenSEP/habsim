"""
examples/cli.py

Example using the CLI.
"""
##
# Copyright (c) 2016 Jerald Thomas
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

from habsim.balloon import hwoyee_1200
from habsim.parachute import Parachute
from habsim.atmosphere import LinearStandardAtmosphere
from habsim.flight import FlightModel

# Create an atmosphere object
# Currently the LinearStandardAtmosphere is the only model implemented
atmosphere = LinearStandardAtmosphere()

# Create a parachute
parachute = Parachute(1.5, 1.5)

# Create a flight model with initial parameters
flight_model = FlightModel(atmosphere, hwoyee_1200, parachute, 2.0, 1.0, 'helium')

# Print initial conditions
print "Balloon Burst Diameter: %fm" % flight_model.balloon.burst_diameter
print "Balloon Drag Coefficient: %f" % flight_model.balloon.drag_coefficient
print "Balloon Mass %fkg" % flight_model.balloon.mass
print "Payload Mass %fkg" % flight_model.payload_mass
print "System Mass %fkg" % (flight_model.balloon.mass + flight_model.payload_mass)
print "Net Mass %fkg" % flight_model.net_mass
print "Launch Volume %fm^3" % flight_model.initial_volume
print "Gas Mass %fkg" % flight_model.gas_mass
print

# Print burst altitude
print "Burst Altitude: %fm" % flight_model.burst_altitude
print

# Print altitude by the minute
for t in range(0, len(flight_model), int(60 / flight_model.timestep)):
    print "Time: %dmin, Altitude: %dm Velocity: %fm/s" % (
    t * flight_model.timestep / 60, flight_model.altitude(t), flight_model.velocity(t))
print

# Print average velocity
sum = 0.0
for data in flight_model[:int(flight_model.burst_time/flight_model.timestep)]:
    sum += data['velocity']
print "Average Rising Velocity: %fm/s" % (sum / len(flight_model))

sum = 0.0
for data in flight_model[int(flight_model.burst_time/flight_model.timestep)+1:]:
    sum += data['velocity']
print "Average Falling Velocity: %fm/s" % (sum / len(flight_model))

