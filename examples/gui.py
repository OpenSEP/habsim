"""
examples/gui.py

Example using the GUI.
"""
##
# Copyright (c) 2016 Jerald Thomas
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

from habsim.gui import HABsimApp
from habsim.balloon import hwoyee_1200
from habsim.parachute import Parachute
from habsim.atmosphere import LinearStandardAtmosphere
from habsim.flight import FlightModel

# Create an atmosphere object
# Currently the LinearStandardAtmosphere is the only model implemented
atmosphere = LinearStandardAtmosphere()

# Create a parachute
# Descent simulation is not yet fully implemented, so this is more of a placeholder
parachute = Parachute(1.5, 1.5)

# Create a flight model with initial parameters
flight_model = FlightModel(atmosphere, hwoyee_1200, parachute, 2.0, 1.0, 'helium')

# Create the GUI object
gui = HABsimApp(flight_model)

# Start the GUI
gui.run()