"""
habsim/gui.py

HABsim GUI.
"""
##
# Copyright (c) 2016 Jerald Thomas
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

from math import ceil

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.slider import Slider
from kivy.garden.graph import Graph, MeshLinePlot, MeshStemPlot

import kivy

kivy.require('1.9.0')


class HABsimApp(App):
    def __init__(self, flight_model):
        Window.maximize()
        self.built = False
        self.flight_model = flight_model

        self.information_graph = None
        self.temperature_graph = None
        self.pressure_graph = None
        self.density_graph = None
        self.altitude_graph = None
        self.volume_graph = None
        self.radius_graph = None
        self.velocity_graph = None
        self.terminal_velocity_graph = None

        self.create_graphs()
        self.graph_container = BoxLayout()
        self.graph = self.altitude_graph
        self.graph_container.add_widget(self.graph)
        self.stem_plot = None
        Window.bind(mouse_pos=self.move_stem_plot)

        self.btn_information = ToggleButton(text="Information", group="graphs")
        self.btn_temperature = ToggleButton(text="Atmosphere Temperature", group="graphs")
        self.btn_pressure = ToggleButton(text="Atmosphere Pressure", group="graphs")
        self.btn_density = ToggleButton(text="Atmosphere Density", group="graphs")
        self.btn_altitude = ToggleButton(text="Flight Altitude", group="graphs", state="down")
        self.btn_volume = ToggleButton(text="Flight Volume", group="graphs")
        self.btn_radius = ToggleButton(text="Flight Radius", group="graphs")
        self.btn_velocity = ToggleButton(text="Flight Velocity", group="graphs")
        self.btn_terminal_velocity = ToggleButton(text="Flight Terminal Velocity", group="graphs")

        self.btn_information.bind(on_press=self.switch_to_information)
        self.btn_temperature.bind(on_press=self.switch_to_temperature)
        self.btn_pressure.bind(on_press=self.switch_to_pressure)
        self.btn_density.bind(on_press=self.switch_to_density)
        self.btn_altitude.bind(on_press=self.switch_to_altitude)
        self.btn_volume.bind(on_press=self.switch_to_volume)
        self.btn_radius.bind(on_press=self.switch_to_radius)
        self.btn_velocity.bind(on_press=self.switch_to_velocity)
        self.btn_terminal_velocity.bind(on_press=self.switch_to_terminal_velocity)

        self.info_label = Label(text="")

        self.payload_mass = Slider(value=self.flight_model.payload_mass, min=0.0, max=20.0)
        self.payload_mass.bind(value=self.update_payload_mass)
        self.payload_mass_label = Label(text="Payload Mass: %fkg" % self.payload_mass.value)

        self.net_mass = Slider(value=self.flight_model.net_mass, min=0.0, max=5.0)
        self.net_mass.bind(value=self.update_net_mass)
        self.net_mass_label = Label(text="Net Mass: %fkg" % self.net_mass.value)

        self.balloon_burst = Slider(value=self.flight_model.balloon.burst_diameter, min=0.0, max=20.0)
        self.balloon_burst_label = Label(text="Balloon Burst Diameter: %fm" % self.balloon_burst.value)

        self.balloon_drag = Slider(value=self.flight_model.balloon.drag_coefficient, min=0.0, max=1.0)
        self.balloon_drag_label = Label(text="Balloon Drag Coefficient: %f" % self.balloon_drag.value)

        self.balloon_mass = Slider(value=self.flight_model.balloon.mass, min=0.0, max=5.0)
        self.balloon_mass_label = Label(text="Balloon Mass: %fkg" % self.balloon_mass.value)

        self.recalculate_btn = Button(text="Recalculate")
        self.recalculate_btn.bind(on_press=self.recalculate)

    def create_graphs(self):
        # Information
        self.information_graph = BoxLayout(orientation="vertical")
        self.information_graph.add_widget(Label(text="Launch Volume: %fm^3" % self.flight_model.initial_volume))
        self.information_graph.add_widget(Label(text="Gas Mass: %fkg" % self.flight_model.gas_mass))
        self.information_graph.add_widget(Label(text="Burst Altitude %fm" % self.flight_model.burst_altitude))

        # Temperature
        self.temperature_graph = Graph(xlabel='Altitude (m)', ylabel='Temperature (K)', x_ticks_minor=1000,
                                       x_ticks_major=10000, y_ticks_major=25,
                                       y_grid_label=True, x_grid_label=True, padding=5,
                                       x_grid=True, y_grid=True, xmin=-0, xmax=80000, ymin=150, ymax=300)

        plot = MeshLinePlot(color=[1, 0, 0, 1])
        plot.points = [(x, self.flight_model.atmosphere.temperature(x)) for x in range(0, 80001, 10)]
        self.temperature_graph.add_plot(plot)

        # Pressure
        self.pressure_graph = Graph(xlabel='Altitude (m)', ylabel='Pressure (kPa)', x_ticks_minor=1000,
                                    x_ticks_major=10000, y_ticks_major=25,
                                    y_grid_label=True, x_grid_label=True, padding=5,
                                    x_grid=True, y_grid=True, xmin=-0, xmax=80000, ymin=0, ymax=150)

        plot = MeshLinePlot(color=[1, 0, 0, 1])
        plot.points = [(x, self.flight_model.atmosphere.pressure(x) / 1000.0) for x in range(0, 80001, 10)]
        self.pressure_graph.add_plot(plot)

        # Density
        self.density_graph = Graph(xlabel='Altitude (m)', ylabel='Density (kg/m^3)', x_ticks_minor=1000,
                                   x_ticks_major=10000, y_ticks_major=0.5,
                                   y_grid_label=True, x_grid_label=True, padding=5,
                                   x_grid=True, y_grid=True, xmin=-0, xmax=80000, ymin=0, ymax=2)

        plot = MeshLinePlot(color=[1, 0, 0, 1])
        plot.points = [(x, self.flight_model.atmosphere.density(x)) for x in range(0, 80001, 10)]
        self.density_graph.add_plot(plot)

        # Altitude
        miny = 0.0
        maxy = ceil(self.flight_model[-1][0])
        minx = 0.0
        maxx = ceil(self.flight_model[-1][-1])
        self.altitude_graph = Graph(xlabel='Time (s)', ylabel='Altitude (m)', x_ticks_minor=1000,
                                    x_ticks_major=5000, y_ticks_major=5000,
                                    y_grid_label=True, x_grid_label=True, padding=5,
                                    x_grid=True, y_grid=True, xmin=minx, xmax=maxx, ymin=miny, ymax=maxy)

        plot = MeshLinePlot(color=[0, 0, 1, 1])
        plot.points = [(x * self.flight_model.timestep, self.flight_model.altitude(x)) for x in
                       range(0, len(self.flight_model) + 1, 10)]
        self.altitude_graph.add_plot(plot)

        # Volume
        miny = 0.0
        maxy = ceil(self.flight_model[-1][1])
        minx = 0.0
        maxx = ceil(self.flight_model[-1][-1])
        self.volume_graph = Graph(xlabel='Time (s)', ylabel='Volume (m^3)', x_ticks_minor=1000,
                                  x_ticks_major=5000, y_ticks_major=100,
                                  y_grid_label=True, x_grid_label=True, padding=5,
                                  x_grid=True, y_grid=True, xmin=minx, xmax=maxx, ymin=miny, ymax=maxy)

        plot = MeshLinePlot(color=[0, 0, 1, 1])
        plot.points = [(x * self.flight_model.timestep, self.flight_model.volume(x)) for x in
                       range(0, len(self.flight_model) + 1, 10)]
        self.volume_graph.add_plot(plot)

        # Radius
        miny = 0.0
        maxy = ceil(self.flight_model.balloon.burst_diameter / 2.0)
        minx = 0.0
        maxx = ceil(self.flight_model[-1][-1])
        self.radius_graph = Graph(xlabel='Time (s)', ylabel='Radius (m)', x_ticks_minor=1000,
                                  x_ticks_major=5000, y_ticks_major=1,
                                  y_grid_label=True, x_grid_label=True, padding=5,
                                  x_grid=True, y_grid=True, xmin=minx, xmax=maxx, ymin=miny, ymax=maxy)

        plot = MeshLinePlot(color=[0, 0, 1, 1])
        plot.points = [(x * self.flight_model.timestep, self.flight_model.radius(x)) for x in
                       range(0, len(self.flight_model) + 1, 10)]
        self.radius_graph.add_plot(plot)

        # Velocity
        miny = self.flight_model.min_velocity
        maxy = self.flight_model.max_velocity
        minx = 0.0
        maxx = ceil(self.flight_model[-1][-1])
        self.velocity_graph = Graph(xlabel='Time (s)', ylabel='Velocity (m/s)', x_ticks_minor=1000,
                                    x_ticks_major=5000, y_ticks_major=1,
                                    y_grid_label=True, x_grid_label=True, padding=5,
                                    x_grid=True, y_grid=True, xmin=minx, xmax=maxx, ymin=miny, ymax=maxy)

        plot = MeshLinePlot(color=[0, 0, 1, 1])
        plot.points = [(x * self.flight_model.timestep, self.flight_model.velocity(x)) for x in
                       range(0, len(self.flight_model) + 1, 10)]
        self.velocity_graph.add_plot(plot)

        # Terminal Velocity
        miny = self.flight_model.min_velocity
        maxy = self.flight_model.max_velocity
        minx = 0.0
        maxx = ceil(self.flight_model[-1][-1])
        self.terminal_velocity_graph = Graph(xlabel='Time (s)', ylabel='Terminal Velocity (m/s)', x_ticks_minor=1000,
                                             x_ticks_major=5000, y_ticks_major=1,
                                             y_grid_label=True, x_grid_label=True, padding=5,
                                             x_grid=True, y_grid=True, xmin=minx, xmax=maxx, ymin=miny, ymax=maxy)

        plot = MeshLinePlot(color=[0, 0, 1, 1])
        plot.points = [(x * self.flight_model.timestep, self.flight_model.terminal_velocity(x)) for x in
                       range(0, len(self.flight_model) + 1, 10)]
        self.terminal_velocity_graph.add_plot(plot)

    def recalculate(self, btn):
        self.flight_model.payload_mass = self.payload_mass.value
        self.flight_model.net_mass = self.net_mass.value
        self.flight_model.balloon.mass = self.balloon_mass.value
        self.flight_model.balloon.burst_diameter = self.balloon_burst.value
        self.flight_model.balloon.drag_coefficient = self.balloon_drag.value
        self.flight_model.calculate()

        self.graph_container.remove_widget(self.graph)

        if self.graph is self.information_graph:
            self.create_graphs()
            self.graph = self.information_graph
        elif self.graph is self.temperature_graph:
            self.create_graphs()
            self.graph = self.temperature_graph
        elif self.graph is self.pressure_graph:
            self.create_graphs()
            self.graph = self.pressure_graph
        elif self.graph is self.density_graph:
            self.create_graphs()
            self.graph = self.density_graph
        elif self.graph is self.altitude_graph:
            self.create_graphs()
            self.graph = self.altitude_graph
        elif self.graph is self.volume_graph:
            self.create_graphs()
            self.graph = self.volume_graph
        elif self.graph is self.radius_graph:
            self.create_graphs()
            self.graph = self.radius_graph
        elif self.graph is self.velocity_graph:
            self.create_graphs()
            self.graph = self.velocity_graph
        elif self.graph is self.terminal_velocity_graph:
            self.create_graphs()
            self.graph = self.terminal_velocity_graph

            self.graph_container.add_widget(self.graph)

    def switch_graph(self, btn, graph):
        if btn.state == 'normal':
            btn.state = 'down'
        else:
            self.graph_container.remove_widget(self.graph)
            self.graph = graph
            self.graph_container.add_widget(self.graph)

    def switch_to_information(self, btn):
        self.switch_graph(btn, self.information_graph)

    def switch_to_temperature(self, btn):
        self.switch_graph(btn, self.temperature_graph)

    def switch_to_pressure(self, btn):
        self.switch_graph(btn, self.pressure_graph)

    def switch_to_density(self, btn):
        self.switch_graph(btn, self.density_graph)

    def switch_to_altitude(self, btn):
        self.switch_graph(btn, self.altitude_graph)

    def switch_to_volume(self, btn):
        self.switch_graph(btn, self.volume_graph)

    def switch_to_radius(self, btn):
        self.switch_graph(btn, self.radius_graph)

    def switch_to_velocity(self, btn):
        self.switch_graph(btn, self.velocity_graph)

    def switch_to_terminal_velocity(self, btn):
        self.switch_graph(btn, self.terminal_velocity_graph)

    def update_balloon_burst(self, slider, val):
        self.balloon_burst_label.text = "Balloon Burst Diameter: %fm" % val

    def update_balloon_drag(self, slider, val):
        self.balloon_drag_label.text = "Balloon Drag Coefficient: %f" % val

    def update_balloon_mass(self, slider, val):
        self.balloon_mass_label.text = "Balloon Mass: %fkg" % val

    def update_payload_mass(self, slider, val):
        self.payload_mass_label.text = "Payload Mass: %fkg" % val

    def update_net_mass(self, slider, val):
        self.net_mass_label.text = "Net Mass: %fkg" % val

    def move_stem_plot(self, window, pos):
        if self.graph is not self.information_graph:
            # Currently we have to access the protected member _plot_area of Graph
            # In the future I would like to add a method to Graph that will given a mouse
            # position return the x and y values of the plot
            graph_pos = [None, None]
            graph_pos[0] = self.graph._plot_area.pos[0] + self.graph_container.pos[0]
            graph_pos[1] = self.graph._plot_area.pos[1] + self.graph_container.pos[1]
            graph_size = self.graph._plot_area.size

            if graph_pos[0] < pos[0] < graph_pos[0] + graph_size[0]:
                if graph_pos[1] < pos[1] < graph_pos[1] + graph_size[1]:

                    min1 = graph_pos[0]
                    max1 = graph_pos[0] + graph_size[0]
                    min2 = self.graph.xmin
                    max2 = self.graph.xmax
                    slope = (max2 - min2) / (max1 - min1)
                    xval = (min2 + slope * (pos[0] - min1))

                    point = None
                    info_text = ""
                    if self.graph is self.temperature_graph:
                        point = (xval, self.flight_model.atmosphere.temperature(int(xval)))
                        info_text = "Altitude: %dm, Temperature: %fK"
                    elif self.graph is self.pressure_graph:
                        point = (xval, self.flight_model.atmosphere.pressure(int(xval)) / 1000.0)
                        info_text = "Altitude: %dm, Pressure: %fkPa"
                    elif self.graph is self.density_graph:
                        point = (xval, self.flight_model.atmosphere.density(int(xval)))
                        info_text = "Altitude: %dm, Density: %fkg/m^3"
                    elif self.graph is self.altitude_graph:
                        point = (xval, self.flight_model.altitude(int(xval / self.flight_model.timestep)))
                        info_text = "Time: %fs, Altitude: %fm"
                    elif self.graph is self.volume_graph:
                        point = (xval, self.flight_model.volume(int(xval / self.flight_model.timestep)))
                        info_text = "Time: %fs, Volume: %fm^3"
                    elif self.graph is self.radius_graph:
                        point = (xval, self.flight_model.radius(int(xval / self.flight_model.timestep)))
                        info_text = "Time: %fs, Radius: %fm"
                    elif self.graph is self.velocity_graph:
                        point = (xval, self.flight_model.velocity(int(xval / self.flight_model.timestep)))
                        info_text = "Time: %fs, Velocity: %fm/s"
                    elif self.graph is self.terminal_velocity_graph:
                        point = (xval, self.flight_model.terminal_velocity(int(xval / self.flight_model.timestep)))
                        info_text = "Time: %fs, Terminal Velocity: %fm/s"

                    if not point:
                        self.stem_plot = None

                    if self.stem_plot:
                        self.graph.remove_plot(self.stem_plot)
                    self.stem_plot = MeshStemPlot(points=[point], color=[1, 1, 1, 1])
                    self.graph.add_plot(self.stem_plot)
                    self.info_label.text = info_text % point

                elif self.stem_plot:
                    self.graph.remove_plot(self.stem_plot)
                    self.stem_plot = None
                    self.info_label.text = ""
            elif self.stem_plot:
                self.graph.remove_plot(self.stem_plot)
                self.stem_plot = None
                self.info_label.text = ""

    def build(self):
        root = BoxLayout()
        left = BoxLayout(orientation="vertical")
        left.add_widget(self.graph_container)

        left_bottom = BoxLayout(orientation="vertical")
        left_bottom.add_widget(self.info_label)
        left_bottom.add_widget(self.payload_mass_label)
        left_bottom.add_widget(self.payload_mass)
        left_bottom.add_widget(self.net_mass_label)
        left_bottom.add_widget(self.net_mass)
        left_bottom.add_widget(self.balloon_burst_label)
        left_bottom.add_widget(self.balloon_burst)
        left_bottom.add_widget(self.balloon_drag_label)
        left_bottom.add_widget(self.balloon_drag)
        left_bottom.add_widget(self.balloon_mass_label)
        left_bottom.add_widget(self.balloon_mass)
        left_bottom.add_widget(self.recalculate_btn)
        left.add_widget(left_bottom)

        right = BoxLayout(orientation="vertical")
        right.add_widget(self.btn_information)
        right.add_widget(self.btn_temperature)
        right.add_widget(self.btn_pressure)
        right.add_widget(self.btn_density)
        right.add_widget(self.btn_altitude)
        right.add_widget(self.btn_volume)
        right.add_widget(self.btn_radius)
        right.add_widget(self.btn_velocity)
        right.add_widget(self.btn_terminal_velocity)

        root.add_widget(left)
        root.add_widget(right)

        return root
