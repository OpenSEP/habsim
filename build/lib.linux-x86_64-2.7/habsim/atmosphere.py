"""
habsim/atmosphere.py

Atmosphere models.
"""
##
# Copyright (c) 2016 Jerald Thomas
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##


class Atmosphere:
    """Empty abstract class"""
    def __init__(self):
        pass


class LinearStandardAtmosphere(Atmosphere):
    """
    U.S. Standard Atmosphere Model linearized between given data points to one meter resolution
    source: <http://www.engineeringtoolbox.com/standard-atmosphere-d_604.html>
    """
    # Altitude Temperature Gravity Pressure Density Viscosity
    standard_atmosphere = """0	15.00	9.807	10.13	12.25	1.789
    1000	8.50	9.804	8.988	11.12	1.758
    2000	2.00	9.801	7.950	10.07	1.726
    3000	-4.49	9.797	7.012	9.093	1.694
    4000	-10.98	9.794	6.166	8.194	1.661
    5000	-17.47	9.791	5.405	7.364	1.628
    6000	-23.96	9.788	4.722	6.601	1.595
    7000	-30.45	9.785	4.111	5.900	1.561
    8000	-36.94	9.782	3.565	5.258	1.527
    9000	-43.42	9.779	3.080	4.671	1.493
    10000	-49.90	9.776	2.650	4.135	1.458
    15000	-56.50	9.761	1.211	1.948	1.422
    20000	-56.50	9.745	0.5529	0.8891	1.422
    25000	-51.60	9.730	0.2549	0.4008	1.448
    30000	-46.64	9.715	0.1197	0.1841	1.475
    40000	-22.80	9.684	0.0287	0.03996	1.601
    50000	-25	9.654	0.007978	0.01027	1.704
    60000	-26.13	9.624	0.002196	0.003097	1.584
    70000	-53.57	9.594	0.00052	0.0008283	1.438
    80000	-74.51	9.564	0.00011	0.0001846	1.321"""

    def __init__(self):
        lines = LinearStandardAtmosphere.standard_atmosphere.split('\n')
        data = []
        for line in lines:
            line = line.split('\t')
            data.append([float(x) for x in line])

        low = int(data[0][0])
        high = int(data[-1][0])

        self._data = []

        floor = 0
        ceiling = 1
        for m in range(low, high+1):
            if m > data[ceiling][0]:
                floor = ceiling
                ceiling += 1

            data_point = [m]
            for i in range(1, len(data[floor])):
                x1 = data[floor][0]
                x2 = data[ceiling][0]
                y1 = data[floor][i]
                y2 = data[ceiling][i]

                data_point.append(((y2 - y1) / (x2 - x1)) * (m - x1) + y1)

            self._data.append(data_point)

        self.lower_bound = self._data[0][0]
        self.upper_bound = self._data[-1][0]

    def temperature(self, m):
        if self.__getitem__(m):
            return self.__getitem__(m)[1] + 273.15
        return None

    def gravity(self, m):
        if self.__getitem__(m):
            return self.__getitem__(m)[2]
        return None

    def pressure(self, m):
        if self.__getitem__(m):
            return self.__getitem__(m)[3] * (10.0 ** 4)
        return None

    def density(self, m):
        if self.__getitem__(m):
            return self.__getitem__(m)[4] * (10.0 ** -1)
        return None

    def viscosity(self, m):
        if self.__getitem__(m):
            return self.__getitem__(m)[5] * (10.0 ** 5)
        return None

    def __getitem__(self, item):
        if item < self.lower_bound or item > self.upper_bound:
            return None
        return self._data[item]


if __name__ == '__main__':
    atmosphere = LinearStandardAtmosphere()

    for i in range(0, 80001, 10000):
        print atmosphere.viscosity(i)




