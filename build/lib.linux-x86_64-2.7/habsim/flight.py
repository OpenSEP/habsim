"""
habsim/flight.py

Flight model
"""
##
# Copyright (c) 2016 Jerald Thomas
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

from math import pi, tanh


class FlightModel:
    """Flight simulation model"""

    atomic_weights = {
        'helium': 4.002602E-3,
        'hydrogen': 1.008E-3,
        'methane': 16.04E-3
    }

    def __init__(self, atmosphere, balloon, parachute, payload_mass, net_mass, gas):
        """
        Flight Model init
        :param atmosphere: atmosphere model
        :type atmosphere: Atmosphere
        :param balloon: balloon model
        :type balloon: Balloon
        :param parachute: parachute model
        :type parachute: Parachute
        :param payload_mass: mass of payload in kg
        :type payload_mass: float
        :param gas: inflation gas ('helium', 'hydrogen', or 'methane')
        :type gas: str
        """
        self.atmosphere = atmosphere
        self.balloon = balloon
        self.parachute = parachute
        self.payload_mass = payload_mass
        self.net_mass = net_mass
        self.gas = gas

        self.initial_volume = None
        self.burst_altitude = None
        self.gas_mass = None
        self._data = []

        self.max_altitude = 0.0
        self.max_velocity = 0.0
        self.min_velocity = 0.0

        self.timestep = 0.1
        self.maxtime = 65000
        self.calculate()

    def _velocity(self, t, m, volume, radius):
        """
        Calculate velocity and terminal velocity
        calculations from <http://www.physicsmyths.org.uk/buoyancy.htm>
        :param t: current time in seconds
        :type t: float
        :param m: current altitude in meters
        :type m: float
        :param volume: volume of balloon in meters cubed
        :type volume: float
        :param radius: radius of balloon in meters
        :type radius: float
        :return: velocity, terminal_velocity
        :rtype: float, float
        """
        area = pi * (radius ** 2)
        displaced_mass = volume * self.atmosphere.density(int(m))
        net_lift = self.atmosphere.gravity(int(m)) * (
            (self.balloon.mass + self.payload_mass + self.net_mass) - displaced_mass)

        factor = 1
        if net_lift < 0:
            net_lift = abs(net_lift)
            factor = -1

        terminal_velocity = ((net_lift / (
            0.5 * self.atmosphere.density(int(m)) * self.balloon.drag_coefficient * area)) ** 0.5) * factor

        velocity = terminal_velocity * tanh((net_lift * t) / (
            (self.balloon.mass + self.payload_mass + self.net_mass + displaced_mass) * terminal_velocity)) * factor

        return terminal_velocity, velocity

    def calculate(self):
        """
        Calculates flight model. Results go in self._data
        :return: None
        """
        time = 0.1
        altitude = 0.0

        self.initial_volume = (self.payload_mass + self.balloon.mass) / self.atmosphere.density(0)
        self._data = []
        self.burst_altitude = None

        R = 8.3144598
        N = (self.atmosphere.pressure(0) * self.initial_volume) / (R * self.atmosphere.temperature(0))
        self.gas_mass = N * FlightModel.atomic_weights[self.gas.lower()]

        while self.burst_altitude is None:
            volume = (N * R * self.atmosphere.temperature(int(altitude))) / self.atmosphere.pressure(int(altitude))
            radius = (0.75 * volume * (1.0 / pi)) ** (1.0 / 3.0)
            if radius > self.balloon.burst_diameter / 2.0 and self.burst_altitude is None:
                self.burst_altitude = altitude
            terminal_velocity, velocity = self._velocity(time, altitude, volume, radius)
            altitude += self.timestep * velocity
            self._data.append([altitude, volume, radius, velocity, terminal_velocity, time])

            if altitude > self.max_altitude:
                self.max_altitude = altitude
            if velocity > self.max_velocity:
                self.max_velocity = velocity
            if velocity < self.min_velocity:
                self.min_velocity = velocity

            time += self.timestep
            if time > self.maxtime:
                self.burst_altitude = 0.0

        self._data.append(self._data[-1])

    def altitude(self, t):
        """
        Return altitude at time t where t is seconds * timestep
        :rtype: float
        """
        return self._data[t][0]

    def volume(self, t):
        """
        Return volume at time t where t is seconds * timestep
        :rtype: float
        """
        return self._data[t][1]

    def radius(self, t):
        """
        Return radius at time t where t is seconds * timestep
        :rtype: float
        """
        return self._data[t][2]

    def velocity(self, t):
        """
        Return velocity at time t where t is seconds * timestep
        :rtype: float
        """
        return self._data[t][3]

    def terminal_velocity(self, t):
        """
        Return terminal velocity at time t where t is seconds * timestep
        :rtype: float
        """
        return self._data[t][4]

    def __len__(self):
        return len(self._data)

    def __getitem__(self, item):
        return self._data[item]

    def __iter__(self):
        for data in self._data:
            yield {
                'altitude': data[0],
                'volume': data[1],
                'radius': data[2],
                'velocity': data[3],
                'terminal_velocity': data[4],
                'time': data[5]
            }
