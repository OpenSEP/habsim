"""
habsim/balloon.py

Holds the Balloon class. Also instantiates numerous instances for various off the shelf balloons.
"""
##
# Copyright (c) 2016 Jerald Thomas
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##


class Balloon:
    """Balloon struct like class"""
    def __init__(self, burst_diameter, drag_coefficient, mass):
        """
        Balloon init
        :param burst_diameter: Burst diameter of balloon
        :param drag_coefficient: Drag coefficient of balloon
        :param mass: Mass of balloon
        """
        self.burst_diameter = burst_diameter
        self.drag_coefficient = drag_coefficient
        self.mass = mass

# Common off the shelf balloons
# data gathered from CUSF cusf-burst-calc/calc.js source
# <https://github.com/cuspaceflight/cusf-burst-calc/blob/master/js/calc.js> (gathered on 07/28/2016)
kaymont_200 = Balloon(3.00, 0.25, 0.20)
kaymont_300 = Balloon(3.78, 0.25, 0.30)
kaymont_350 = Balloon(4.12, 0.25, 0.35)
kaymont_450 = Balloon(4.72, 0.25, 0.45)
kaymont_500 = Balloon(4.99, 0.25, 0.50)
kaymont_600 = Balloon(6.02, 0.30, 0.60)
kaymont_700 = Balloon(6.53, 0.30, 0.70)
kaymont_800 = Balloon(7.00, 0.30, 0.80)
kaymont_1000 = Balloon(7.86, 0.30, 1.00)
kaymont_1200 = Balloon(8.63, 0.25, 1.20)
kaymont_1500 = Balloon(9.44, 0.25, 1.50)
kaymont_2000 = Balloon(10.54, 0.25, 2.00)
kaymont_3000 = Balloon(13.00, 0.25, 3.00)

hwoyee_100 = Balloon(2.00, .25, 0.10)
hwoyee_300 = Balloon(3.80, .25, 0.30)
hwoyee_350 = Balloon(4.10, .25, 0.35)
hwoyee_400 = Balloon(4.50, .25, 0.40)
hwoyee_500 = Balloon(5.00, .25, 0.50)
hwoyee_600 = Balloon(5.80, .30, 0.60)
hwoyee_750 = Balloon(6.50, .30, 0.75)
hwoyee_800 = Balloon(6.80, .30, 0.80)
hwoyee_950 = Balloon(7.29, .30, 0.95)
hwoyee_1000 = Balloon(7.50, .30, 1.00)
hwoyee_1200 = Balloon(8.50, .25, 1.20)
hwoyee_1500 = Balloon(9.50, .25, 1.50)
hwoyee_1600 = Balloon(10.50, .25, 1.60)
hwoyee_2000 = Balloon(11.00, .25, 2.00)
hwoyee_3000 = Balloon(12.50, .25, 3.00)

pawan_100 = Balloon(1.6, .25, 0.10)
pawan_350 = Balloon(4.0, .25, 0.35)
pawan_600 = Balloon(5.8, .30, 0.60)
pawan_800 = Balloon(6.6, .30, 0.80)
pawan_900 = Balloon(7.0, .30, 0.90)
pawan_1200 = Balloon(8.0, .25, 1.20)
pawan_1600 = Balloon(9.5, .25, 1.60)
pawan_2000 = Balloon(10.2, .25, 2.00)
